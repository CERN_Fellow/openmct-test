/**
 * Created by lmolinar on 02/06/17.
 */
define([
    'openmct',
    './src/TelemetryAdapter',
    // './src/TelemetryInitializer',
    './src/TelemetryModelProvider',
    './src/TelemetryProvider'
], function (
    openmct,
    TelemetryAdapter,
    // TelemetryInitializer,
    TelemetryModelProvider,
    TelemetryProvider
) {
    openmct.legacyRegistry.register("cern_plugin/telemetryDisplay", {
        "name": "Japc Telemetry Plugin",
        "descriptions": "Display a chart for a given device/property#field selector.",
        "extensions": {
            "types": [
                {
                    "name": "Device Telemetry",
                    "key": "cern.telemetry",
                    "cssClass": "icon-telemetry",
                    "features": ["creation"],
                    "properties": [
                        {
                            "key": "deviceName",
                            "name": "Device Name",
                            "control": "textfield"
                        },
                        {
                            "key": "property",
                            "name": "Property",
                            "control": "textfield"
                        },
                        {
                            "key": "field",
                            "name": "Field",
                            "control": "textfield"
                        },
                        {
                            "key": "selector",
                            "name": "Selector",
                            "control": "textfield"
                        }
                    ],
                    "model": { "telemetry": {} },
                    "telemetry": {
                        "source": "cern.japc",
                        "domains": [
                            {
                                "name": "Time",
                                "key": "timestamp"
                            },
                            {
                                "name": "Value",
                                "key": "value"
                            }
                        ],
                        "ranges": [{
                            "key": "value",
                            "name": "Value",
                            "units": "Volts",
                            "format": "number"
                        }]
                    }
                }
            ],
            "services": [
                {
                    "key": "cern.telemetry.adapter",
                    "implementation": TelemetryAdapter,
                    "depends": ["WS_URL", "$q"]
                }
            ],
            "constants": [
                {
                    "key": "WS_URL",
                    "priority": "fallback",
                    "value": "ws://cwe-513-vol327:8899/openMCT/telemetry"
                }
            ],
            // "runs": [
            //     {
            //         "implementation": TelemetryInitializer,
            //         "depends": [ "cern.telemetry.adapter", "objectService" ]
            //     }
            // ],
            "components": [
                {
                    "provides": "modelService",
                    "type": "provider",
                    "implementation": TelemetryModelProvider,
                    "depends": ["$q"]
                },
                {
                    "provides": "telemetryService",
                    "type": "provider",
                    "implementation": TelemetryProvider,
                    "depends": [ "cern.telemetry.adapter", " $q"]
                }
            ]
        }
    });
});