/**
 * Created by lmolinar on 08/06/17.
 */
define(
    [],
    function () {
        "use strict";

        function TelemetryAdapter(url, $q) {
            var ws = new WebSocket(url),
                name,
                histories = {},
                listeners = [];

            ws.onmessage = function (event) {
                var message = JSON.parse(event.data);

                listeners.forEach(function (l) {
                    l(message);
                });
            };

            return {
                history: function (id) {
                    histories[id] = histories[id] || $q.defer();
                    return histories[id].promise;
                },
                subscribe: function (id) {
                    ws.send("sub__"+id);
                },
                unsubscribe: function (id) {
                    ws.send("unsub__ " + id);
                },
                listen: function (callback) {
                    listeners.push(callback);
                },
                setName: function(n){
                    name = n;
                }
            };
        }

        return TelemetryAdapter;
    }
);