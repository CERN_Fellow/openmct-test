/**
 * Created by lmolinar on 09/06/17.
 */
/*global define*/

define(
    ['./TelemetrySeries'],
    function (TelemetrySeries) {
        "use strict";
        var SOURCE = "cern.japc";

        function TelemetryProvider(adapter, $q) {
            var subscribers = {};

            // Used to filter out requests for telemetry
            // from some other source
            function matchesSource(request) {
                // console.log(request);
                console.log(request.source, SOURCE, request.source === SOURCE);
                return (request.source === SOURCE);
            }

            // Listen for data, notify subscribers
            adapter.listen(function (message) {
                // console.log(message);
                var packaged = {};
                packaged[SOURCE] = {};
                packaged[SOURCE][message.id] =
                    new TelemetrySeries([message.value]);
                (subscribers[message.id] || []).forEach(function (cb) {
                    cb(packaged);
                });
            });

            return {
                requestTelemetry: function (requests) {
                    var packaged = {},
                        relevantReqs = requests.filter(matchesSource);

                    // Package historical telemetry that has been received
                    function addToPackage(history) {
                        packaged[SOURCE][history.id] =
                            new TelemetrySeries(history.value);
                    }

                    // Retrieve telemetry for a specific measurement
                    function handleRequest(request) {
                        var key = request.key;
                        return adapter.history(key).then(addToPackage);
                    }

                    packaged[SOURCE] = {};
                    return $q.all(relevantReqs.map(handleRequest))
                        .then(function () { return packaged; });
                },
                subscribe: function (callback, requests) {
                    var keys = requests.filter(matchesSource)
                        .map(function (req) {return req.key;});

                    function notCallback(cb) {
                        return cb !== callback;
                    }

                    function unsubscribe(key) {
                        subscribers[key] =
                            (subscribers[key] || []).filter(notCallback);
                        if (subscribers[key].length < 1) {
                            adapter.unsubscribe(key);
                        }
                    }

                    keys.forEach(function (key) {
                        subscribers[key] = subscribers[key] || [];
                        setTimeout(function(){
                            adapter.subscribe(key);
                        },100);
                        subscribers[key].push(callback);
                    });

                    return function () {
                        keys.forEach(unsubscribe);
                    };
                }
            };
        }
        return TelemetryProvider;
    }
);