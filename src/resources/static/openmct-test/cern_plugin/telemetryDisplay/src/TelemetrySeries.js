/**
 * Created by lmolinar on 09/06/17.
 */
/*global define*/

define(
    function () {
        "use strict";

        function TelemetrySeries(data) {
            console.log(data);
            return {
                getPointCount: function () {
                    return data.length;
                },
                getDomainValue: function (index) {
                    console.log(index);
                    return (data[index] || {}).timestamp;
                },
                getRangeValue: function (index) {
                    return (data[index] || {}).value;
                }
            };
        }
        return TelemetrySeries;
    }
);