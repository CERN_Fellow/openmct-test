/**
 * Created by lmolinar on 09/06/17.
 */
/*global define*/

define(
    function () {
        "use strict";
        function TelemetryInitializer(adapter, objectService) {

            function initialize() {

                function getObject(domainObjects) {
                    return domainObjects["cern_tlm:japc"];
                }

                function populateModel(object) {
                    return object.useCapability(
                        "mutation",
                        function (model) {
                            adapter.setName(model.deviceName);
                        }
                    );
                }

                objectService.getObjects(["cern_tlm:japc"])
                    .then(getObject)
                    .then(populateModel);

            }

            initialize();
        }

        return TelemetryInitializer;
    }
);