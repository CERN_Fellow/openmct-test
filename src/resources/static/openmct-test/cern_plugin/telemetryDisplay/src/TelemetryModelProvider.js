/**
 * Created by lmolinar on 08/06/17.
 */
/*global define*/

define(
    function () {
        "use strict";
        var PREFIX = "cern_tlm:";

        function TelemetryModelProvider($q) {
            var model, empty = $q.when({});

            function isRelevant(id) {
                return id.indexOf(PREFIX) === 0;
            }

            function makeId(element) {
                return PREFIX + element.identifier;
            }

            function getDevicePropertyField(device, property, field, selector) {
                return {
                    deviceName: device,
                    property: property,
                    field: field,
                    selector: selector,
                    id: device + "/" + property + "#" + field + '__' + selector
                };
            }

            function buildModels(){
                var models = {};

                function addTelemetry(element) {
                    models[makeId(element)] = {
                        type: "cern.telemetry",
                        name: element.deviceName,
                        telemetry: {
                            key: element.identifier, //device.id,
                            ranges: [{
                                key: "value",
                                name: "Value",
                                units: "A",
                                format: "number"
                            }]
                        }
                    };
                }
                // getDevicePropertyField("PI.BSW40", "Acquisition", "current", "CPS.USER.ALL")
                addTelemetry({identifier:"test", deviceName:"test Name"});
                return models;
            }

            model = buildModels();
            return {
                getModels: function (ids) {
                    // console.log(ids, ids.some(isRelevant));
                    return model; //ids.some(isRelevant) ? model : empty;
                }
            };
        }
        return TelemetryModelProvider;
    }
);