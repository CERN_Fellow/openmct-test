define([
    'openmct',
    './src/TelemetryAdapter',
    './src/TelemetryInitializer',
    './src/TelemetryModelProvider',
    './src/TelemetryProvider'

], function (
    openmct,
    ExampleTelemetryServerAdapter,
    ExampleTelemetryInitializer,
    ExampleTelemetryModelProvider,
    ExampleTelemetryProvider
) {
    openmct.legacyRegistry.register("cern_plugin/realTimeTelemetry", {
    "name": "Example Telemetry Adapter",
    "extensions": {
        "types": [
            {
                "name": "Cern Devices",
                "key": "example.devices",
                "cssClass": "icon-object"
            },
            {
                "name": "Subsystem",
                "key": "example.subsystem",
                "cssClass": "icon-object",
                "model": { "composition": [] }
            },
            {
                "name": "Measurement",
                "key": "example.measurement",
                "cssClass": "icon-telemetry",
                "model": { "telemetry": {} },
                "telemetry": {
                    "source": "example.source",
                    "domains": [
                        {
                            "name": "Time",
                            "key": "timestamp"
                        }
                    ]
                }
            }
        ],
        "roots": [
            {
                "id": "example:cd",
                "priority": "preferred"
            }
        ],
        "models": [
            {
                "id": "example:cd",
                "model": {
                    "type": "example.devices",
                    "name": "CERN Devices",
                    "location": "ROOT",
                    "composition": []
                }
            }
        ],
        "services": [
            {
                "key": "example.adapter",
                "implementation": ExampleTelemetryServerAdapter,
                "depends": [ "$q", "EXAMPLE_WS_URL" ]
            }
        ],
        "constants": [
            {
                "key": "EXAMPLE_WS_URL",
                "priority": "fallback",
                "value": "ws://cwe-513-vol327:8899/openMCT/telemetry"
            }
        ],
        "runs": [
            {
                "implementation": ExampleTelemetryInitializer,
                "depends": [ "example.adapter", "objectService" ]
            }
        ],
        "components": [
            {
                "provides": "modelService",
                "type": "provider",
                "implementation": ExampleTelemetryModelProvider,
                "depends": [ "example.adapter", "$q" ]
            },
           {
               "provides": "telemetryService",
               "type": "provider",
               "implementation": ExampleTelemetryProvider,
               "depends": [ "example.adapter", "$q" ]
           }
        ]
        }
    });
});
