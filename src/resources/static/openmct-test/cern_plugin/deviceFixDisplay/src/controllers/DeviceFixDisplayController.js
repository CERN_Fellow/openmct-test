/**
 * Created by lmolinar on 02/06/17.
 */
define(function () {
        var socket = new WebSocket('ws://cwe-513-vol327:8899/openMCT/telemetry');

        function DeviceFixDisplayController($scope) {

            function resetScope(){
                $scope.deviceName = $scope.property = $scope.field = $scope.selector = "";
            }

            function getDevicePropertyField(device, property, field, selector) {
                console.log(device,  property, field, selector);
                return {
                    deviceName: device,
                    property: property,
                    field: field,
                    selector: selector,
                    id: device + "/" + property + "#" + field + '__' + selector
                };
            }

            function getIndex(deviceList, id) {
                for (var i = 0; i < deviceList.length; i++) {
                    if (deviceList[i].id == id) {
                        return i;
                    }
                }
                return -1;
            }

            function existsAlready(model, device) {
                var size = model.deviceList.filter(function (d) {
                    return d.id == device.id;
                }).length;
                return size > 0;
            }

            function persist() {
                var persistence = $scope.domainObject.getCapability('persistence');
                return persistence && persistence.persist();
            }

            $scope.removeDeviceProperty = function (device) {
                $scope.domainObject.useCapability('mutation', function
                    (model) {
                    unsubscribe(device.id);
                    model.deviceList.splice(getIndex(model.deviceList, device.id), 1);
                });
                persist();
            };

            function subscribe(id) {
                socket.send('sub__' + id);
            }

            function unsubscribe(id){
                socket.send('unsub__' + id);
            }


            resetScope();

            $scope.addDeviceProperty = function () {
                $scope.domainObject.useCapability('mutation', function
                    (model) {
                    var device = getDevicePropertyField($scope.deviceName, $scope.property, $scope.field, $scope.selector);
                    if (device.deviceName && device.property && device.field && device.selector && !existsAlready(model, device)) {
                        subscribe(device.id);
                        model.deviceList.push(device);
                        resetScope();
                    }
                });
            };

            socket.onmessage = function (message) {
                var data = JSON.parse(message.data);
                $scope.domainObject.useCapability('mutation', function
                    (model) {

                    var index = getIndex(model.deviceList, data.id);
                    if(index>-1){
                        model.deviceList[index].value = data.value;
                        model.deviceList[index].selector = data.selector;
                    }
                });
            };

            $scope.domainObject.useCapability('mutation', function (model) {
                if (model.deviceName && model.property && model.field && model.selector) {
                    var device = getDevicePropertyField(model.deviceName, model.property, model.field, model.selector);
                    model.deviceList.push(device);
                    subscribe(device.id);
                    delete model["deviceName"];
                    delete model["property"];
                    delete model["field"];
                    delete model["selector"];
                }

                model.deviceList.forEach(function (d) {
                    if (d.id) {
                        subscribe(d.id);
                    }
                });
            });
        }

        return DeviceFixDisplayController;
    }
);