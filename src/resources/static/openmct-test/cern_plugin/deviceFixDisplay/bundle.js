/**
 * Created by lmolinar on 02/06/17.
 */

define([
    'openmct',
    './src/controllers/DeviceFixDisplayController'
], function (openmct, DeviceFixDisplayController) {
    openmct.legacyRegistry.register("cern_plugin/deviceFixDisplay", {
        "name": "Fix Display Plugin",
        "descriptions": "Display a fix display for a given property#field.",
        "extensions": {
            "types": [
                {
                    "key": "cern_plugin.deviceFixDisplay",
                    "name": "Device Fix Display",
                    "editable":false,
                    "cssClass": "icon-object",
                    "description": "Display a fix display for a given property#field.",
                    "features": ["creation"],
                    "properties": [
                        {
                            "key": "deviceName",
                            "name": "Device Name",
                            "control": "textfield"
                        },
                        {
                            "key": "property",
                            "name": "Property",
                            "control": "textfield"
                        },
                        {
                            "key": "field",
                            "name": "Field",
                            "control": "textfield"
                        },
                        {
                            "key": "selector",
                            "name": "Selector",
                            "control": "textfield"
                        }
                    ],
                    "model": {
                        "deviceList":[]
                    }
                }
            ],
            "views": [
                {
                    "key": "cern_plugin.deviceFixDisplay",
                    "type": "cern_plugin.deviceFixDisplay",
                    "editable": false,
                    "templateUrl": "templates/deviceFixDisplay.html",
                    "toolbar": {
                        "sections": [
                            {
                                "items": [
                                    {
                                        "text": "Add Device",
                                        "cssClass": "icon-plus",
                                        "method": "addDeviceProperty",
                                        "control": "button"
                                    }
                                ]
                            },
                            {
                                "items": [
                                    {
                                        "cssClass": "icon-trash",
                                        "method": "removeDeviceProperty",
                                        "control": "button"
                                    }
                                ]
                            }
                        ]
                    }
                }
            ],
            "controllers": [
                {
                    "key": "DeviceFixDisplayController",
                    "implementation": DeviceFixDisplayController,
                    "depends": ["$scope"]
                }
            ]
        }
    });
});
