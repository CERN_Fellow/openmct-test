/**
 * Created by lmolinar on 02/06/17.
 */
define([
    'openmct'
], function (openmct) {
    openmct.legacyRegistry.register("cern_plugin/deviceRoot", {
        "name": "Monitored devices",
        "description": "Defines a root folder named Monitored devices",
        "extensions": {
            "roots": [
                {
                    "id": "cern:deviceRoot",
                    "priority": "preferred"
                }
            ],
            "models": [
                {
                    "id": "cern:deviceRoot",
                    "model": {
                        "name": "Monitored devices",
                        "type": "folder",
                        "composition": [],
                        "location": "ROOT"
                    }
                }
            ]
        }
    });
});