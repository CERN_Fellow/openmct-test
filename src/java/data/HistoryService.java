/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package data;

import static java.util.Collections.emptyList;
import static java.util.Collections.synchronizedList;
import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class HistoryService {
    private static final Map<String, List<DataPoint>> historyByDeviceName = new ConcurrentHashMap<>();

    private HistoryService() {
    }

    public static void registerValue(DataPoint value) {
        historyByDeviceName.putIfAbsent(value.getId(), synchronizedList(new ArrayList<>()));
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, -30);
        Date date = calendar.getTime();
        if (!historyByDeviceName.get(value.getId()).isEmpty()
                && historyByDeviceName.get(value.getId()).get(0).getTimestamp() < date.getTime()) {
            historyByDeviceName.get(value.getId()).remove(0);
        }
        historyByDeviceName.get(value.getId()).add(value);
    }

    public static List<DataPoint> getHistory(String propertyField) {
        List<DataPoint> history = historyByDeviceName.get(propertyField);
        if (history != null) {
            return synchronizedList(history.stream().sorted((a,b)->Long.compare(a.getTimestamp(), b.getTimestamp()))// .filter(e -> e.getTimestamp() >= timeFrom && e.getTimestamp() <=
                                                    // timeTo)
                    .collect(toList()));
        }
        return emptyList();
    }

    public static void clearHistory(String propertyField) {
        historyByDeviceName.get(propertyField).clear();
    }
}
