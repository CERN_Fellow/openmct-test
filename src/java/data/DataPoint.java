/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package data;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class DataPoint {
    private final long timestamp;
    private final Object value;
    private final String id;
    private final String deviceName;
    private final String property;
    private final String field;
    private final String selector;
    private final String type = "data";

    public DataPoint(long timestamp, Object value, String id, String deviceName, String property, String field,
            String selector) {
        this.timestamp = timestamp;
        this.value = value;
        this.id = id;
        this.deviceName = deviceName;
        this.property = property;
        this.field = field;
        this.selector = selector;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public Object getValue() {
        return value;
    }

    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public String getProperty() {
        return property;
    }

    public String getField() {
        return field;
    }

    public String getSelector() {
        return selector;
    }

    public String toJson() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            return "";
        }
    }

    @Override
    public String toString() {
        return "DataPoint [timestamp=" + timestamp + ", value=" + value + ", id=" + id + ", deviceName=" + deviceName
                + ", property=" + property + ", field=" + field + ", selector=" + selector + ", type=" + type + "]";
    }

}
