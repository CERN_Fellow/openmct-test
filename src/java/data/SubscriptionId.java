/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package data;

import java.util.Objects;

import org.springframework.web.socket.WebSocketSession;

public class SubscriptionId {
    private final String id;
    private final WebSocketSession session;

    public SubscriptionId(String id, WebSocketSession session) {
        this.id = id;
        this.session = session;
    }

    public String getId() {
        return id;
    }

    public WebSocketSession getSession() {
        return session;
    }
    

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SubscriptionId subscriptionId = (SubscriptionId) o;
        return Objects.equals(id, subscriptionId.id) &&
                Objects.equals(session, subscriptionId.session);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, session);
    }
}
