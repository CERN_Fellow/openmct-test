/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package server;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import data.HistoryService;

@Service
public class WebSocketConnectionHandler extends TextWebSocketHandler {
    @Autowired
    private JapcSubscriberService japcSubscriber;

    private final Set<WebSocketSession> sessions = new HashSet<>();

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        // The WebSocket has been closed
        sessions.remove(session);
        japcSubscriber.unsubscribe();
        System.out.println("Connection closed: " + session.getRemoteAddress());
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        // The WebSocket has been opened
        sessions.add(session);
        System.out.println("Connection started: " + session.getRemoteAddress());
    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage textMessage) throws Exception {
        // A message has been received
        System.out.println("Message received from " + session.getRemoteAddress() + " -> " + textMessage.getPayload());
        String messagePayload = textMessage.getPayload();
        if (messagePayload.contains("sub__") || messagePayload.contains("unsub__")
                || messagePayload.contains("history__")) {
            String[] splitMessage = messagePayload.split("__");
            String command = splitMessage[0];
            String propertyName = splitMessage[1];
            String selector = splitMessage[2];

            switch (command) {
            case "sub":
                subscribe(propertyName, selector, session);
                break;
            case "unsub":
                // japcSubscriber.unsubscribe(propertyName, selector, session);
                break;
            case "history":
                history(propertyName, selector, session);
            }
        } else {
            if (messagePayload.contains("dictionary")) {
                StringBuilder fileContent = new StringBuilder();
                URL url = getClass().getResource("dictionary.json");
                File file = new File(url.getPath());

                try (BufferedReader br = new BufferedReader(new FileReader(file))) {
                    String sCurrentLine;
                    while ((sCurrentLine = br.readLine()) != null) {
                        fileContent.append(sCurrentLine);
                    }
                    Map<String, Object> map = new HashMap<>();
                    map.put("type", "dictionary");
                    map.put("value", fileContent.toString());
                    String message = HistoryController.convertToJson(map).replace("\\\"", "\"").replace("\"{", "{")
                            .replace("}\"", "}");
                    session.sendMessage(new TextMessage(message));
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
    }

    private void subscribe(String propertyName, String selector, WebSocketSession session) {
        try {
            japcSubscriber.subscribe(propertyName, selector, session);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void history(String propertyName, String selector, WebSocketSession session) {
        Map<String, Object> map = new HashMap<>();
        String propertyId = propertyName + "__" + selector;
        map.put("type", "history");
        map.put("id", propertyId);
        map.put("value", HistoryService.getHistory(propertyId));
        String message = HistoryController.convertToJson(map).replace("\\\"", "\"").replace("\"{", "{")
                .replace("}\"", "}").replace("\"[", "[").replace("]\"", "]");
        try {
            session.sendMessage(new TextMessage(message));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}