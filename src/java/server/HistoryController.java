/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package server;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import data.HistoryService;

@Controller
public class HistoryController {

    @RequestMapping(value = { "/restAPI/history" }, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Object> getTelemetryData(@RequestParam(value = "start", required = true) long start,
            @RequestParam(value = "end", required = true) long end,
            @RequestParam(value = "propertyName", required = true) String propertyName) {

        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_JSON);
        String response = convertToJson(HistoryService.getHistory(propertyName));//, start, end));
        return new ResponseEntity<>(response, header, HttpStatus.OK);

    }

    public static String convertToJson(Object object) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            return "";
        }
    }
}
