/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package server;

import java.io.IOException;

import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import cern.japc.AcquiredParameterValue;
import cern.japc.ParameterException;
import cern.japc.ParameterValueListener;
import data.DataPoint;
import data.HistoryService;

public class JapcListener implements ParameterValueListener {
    private final WebSocketSession session;
    private final String id;

    public JapcListener(WebSocketSession session, String id) {
        this.session = session;
        this.id = id;
    }

    @Override
    public void valueReceived(String parameterName, AcquiredParameterValue value) {
        try {
            String stringValue = value.getValue().getString();
            DataPoint dp = new DataPoint(value.getHeader().getAcqStampMillis(), stringValue, id, getDeviceName(parameterName),
                    getProperty(parameterName), getField(parameterName), value.getHeader().getSelector().getId());
            HistoryService.registerValue(dp);
            String jsonObj = dp.toJson();
            synchronized (session) {
                System.out.println(" [" + jsonObj +"] ");
                session.sendMessage(new TextMessage(jsonObj));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getDeviceName(String parameterName) {
        return parameterName.split("/")[0];
    }

    private String getProperty(String parameterName) {
        return parameterName.split("/")[1].split("#")[0];
    }

    private String getField(String parameterName) {
        return parameterName.split("/")[1].split("#")[1];
    }

    @Override
    public void exceptionOccured(String parameterName, String description, ParameterException exception) {
        System.out.println(" [" + exception.getMessage() + "] ");
    }
}
