/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package server;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.WebSocketSession;

import cern.japc.Parameter;
import cern.japc.ParameterException;
import cern.japc.ParameterValueListener;
import cern.japc.Selector;
import cern.japc.SubscriptionHandle;
import cern.japc.factory.ParameterFactory;
import cern.japc.factory.SelectorFactory;
import data.SubscriptionId;

@Service
@Scope(value = "singleton")
public class JapcSubscriberService {
    private Map<SubscriptionId,SubscriptionHandle> subscriptionHandlers = new ConcurrentHashMap<>();

    public void subscribe(String propertyName, String selector, WebSocketSession session) throws ParameterException {
        SubscriptionId id = getId(propertyName,selector, session);
        Parameter p = ParameterFactory.newInstance().newParameter(propertyName);
        Selector s = SelectorFactory.newSelector(selector);
        ParameterValueListener pvl = new JapcListener(session, getStringId(propertyName, selector));

        subscriptionHandlers.putIfAbsent(id, p.createSubscription(s, pvl));
        subscriptionHandlers.get(id).startMonitoring();
    }

    public void unsubscribe(String propertyName, String selector, WebSocketSession session) {
        subscriptionHandlers.get(getId(propertyName,selector, session)).stopMonitoring();
    }
    
    public void unsubscribe() {
        subscriptionHandlers.values().forEach(sh -> sh.stopMonitoring());
        subscriptionHandlers.clear();
    }
    
    private String getStringId(String propertyName, String selector) {
        return propertyName+"__"+selector;
    }
    
    private SubscriptionId getId(String propertyName, String selector, WebSocketSession session) {
        return new SubscriptionId(getStringId(propertyName, selector), session);
    }
}
